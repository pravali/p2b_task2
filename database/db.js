const mongoose = require('mongoose');
const config = require('../config/config');

const dbConnect = async () => {
    console.log("Connecting to Db.....")
    const mongoURL = config.MONGO_URI;
    // console.log(mongoURL)
    await mongoose.connect(mongoURL, { useNewUrlParser: true, useUnifiedTopology: true }).then(
        () => { console.log("Connected") },
        err => { console.log(err) }
    );
}

module.exports = {
    dbConnect
}