const mongoose = require('mongoose');
const tableName = `users`;

const userSchema = mongoose.Schema({
    userId: String,
	username: String,
	password: String,
	createdAt:String,
});
module.exports = mongoose.model(tableName, userSchema, tableName);