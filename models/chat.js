const mongoose = require('mongoose');
const tableName = `chats`;

const userSchema = mongoose.Schema({
    chatId: String,
	groupName: String,
	createdAt:String,
});
module.exports = mongoose.model(tableName, userSchema, tableName);