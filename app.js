const express = require('express')
const logger = require('morgan');
const cors = require('cors');
const bodyParser = require('body-parser');
const http = require('http')
const path = require('path')
const socketio = require('socket.io')

const app = express()
const server = http.createServer(app)
const io = socketio(server)

// Middle wares
app.use(cors());
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

// app.set('view engine', 'ejs');
app.use(express.static(path.join(__dirname, 'public')))

io.on('connection', socket => {
    console.log("New connection")
    // socket.emit('message', "Welcome to chat")

    socket.on("joined", ({ username, group }) => {
        socket.join(group);
    });
    

    socket.on("chatMessage", (msg) => {
        console.log(msg)
        // io.emit("message", msg);
        io.to(msg.group).emit("cmessage", msg);
    });
})

app.get('/', (req, res) => {
    res.render('pages/index')
})

app.post('/test', (req, res) => {
    console.log(req.body)
})

app.get('/chat', (req, res) => {
    res.render('pages/chat')
})

//Route variables
const user_management = require('./routes/user_management');
const chat = require('./routes/chats');

//Routes
app.use('/', user_management);
app.use('/', chat);

server.listen(3000, () =>{
	console.log("Server running on port 3000")
})

// module.exports = app;