const { username, group } = Qs.parse(location.search, {
    ignoreQueryPrefix: true,
});

const socket = io();

socket.emit('joined', { username, group });

socket.on('message', message => {
    console.log(message)

    let html = `<div>
                    <h6>User4</h2>
                    <p>${message}</p>
                </div>`
    $('.container').append(html)
})

socket.on('cmessage', chat => {
    console.log("message from server", chat)

    let html = `<div>
                    <h6>${chat.username}</h2>
                    <p>${chat.message}</p>
                </div>`
    $('.container').append(html)
})

$("form").submit(function( event ) {
    let message = $("#message").val()
    console.log(message)
    if(message != ""){
        socket.emit("chatMessage",{username:username, message:message, group:group})
    }else{
        $("span").text("Please enter message!").show().fadeOut( 1000 );
    }
    event.preventDefault();
    $("#message").val("")
});