const router = require("express").Router();
const randomstring = require('randomstring');
const moment = require('moment-timezone');
const config = require('../config/config');
const { dbConnect } = require('../database/db');

// Models
const Chat = require("../models/chat")

router.post("/create/group", async (req, res) => {
	try {
        // Current date and time
        var now1 = Date.now();
        var datetime = moment(now1).tz('Asia/Kolkata').format('YYYY/MM/DD HH:mm:ss');
        console.log(datetime);

		const { groupName } = req.body;
		if(groupName){
            await dbConnect()
            const chat = await Chat.findOne({groupName:groupName})
            if(chat){
                return res.send({status:"failure", message:"groupName already exist"})
            }else{
                let chatId = randomstring.generate({
                    length: 12,
                    charset: 'alphanumeric'
                }); 
                let params = {
                    chatId,
                    groupName,
                    createdAt: datetime
                }
                await Chat.insertMany(params)
                return res.send({status:"success", message:"Group created!"})
            }
		}else{
			return res.send({status:'failure',message:"groupName can't be null"});
		}
	}catch(error) {
		return res.send({status:"failure", message:error.message})
	}
})

router.get("/list/groups", async (req, res) => {
	try {
        await dbConnect()
        const groups = await Chat.find({})
        return res.send({status:"success", data: groups})
	}catch(error) {
		return res.send({status:"failure", message:error.message})
	}
})

module.exports = router