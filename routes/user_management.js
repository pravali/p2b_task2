const router = require("express").Router();
const bcrypt = require('bcryptjs');
const randomstring = require('randomstring');
const moment = require('moment-timezone');
const config = require('../config/config');
const { dbConnect } = require('../database/db');

// Models
const User = require("../models/user")

router.post("/signup", async (req, res) => {
	try {
        // Current date and time
        var now1 = Date.now();
        var datetime = moment(now1).tz('Asia/Kolkata').format('YYYY/MM/DD HH:mm:ss');
        console.log(datetime);

		const { username, password } = req.body;
		if(username && password){
            await dbConnect()
            const user = await User.findOne({username:username})
            if(user){
                return res.send({status:"failure", message:"username already exist"})
            }else{
                let userId = randomstring.generate({
                    length: 12,
                    charset: 'alphanumeric'
                }); 
                const salt = await bcrypt.genSalt(10);	
                const hashnewpwd = await bcrypt.hash(password, salt)
                let params = {
                    userId,
                    username,
                    password: hashnewpwd,
                    createdAt: datetime
                }
                await User.insertMany(params)
                return res.send({status:"success", message:"Signup success!"})
            }
		}else {
			return res.send({status:'failure',message:"username/password can't be null"});
		}
	}catch(error) {
		return res.send({status:"failure", message:error.message})
	}
})

router.post("/login", async (req, res) => {
	try {
		const { username, password } = req.body;
		if(username && password){
            await dbConnect()
            const user = await User.findOne({username:username})
			const hashPassword = user.password;
			const result = await bcrypt.compare(password, hashPassword)
			if(!result){
				return res.send({status:"failure", message:"Incorrect Password!"})
			}else{
                const result = {
                    status:"success",
                    message:'Successfully logged in!',
                    username:`${user.username}`,
                    userId:`${user.userId}`,
                }
                return res.status(200).send(result);	
			}
		}else {
			return res.send({status:'failure',message:"username/password can't be null"});
		}
	}catch(error) {
		return res.send({status:"failure", message:error.message})
	}
})

module.exports = router